# 5D Chess Discord

Discord bot to generate 5D Chess boards based 5DPGN.

## Config

When looking to configure the bot, save the json file as `config.json`.

Example configuration file:

```
{
  "prefix": "$",
  "token": "<token here>"
}
```

## Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
