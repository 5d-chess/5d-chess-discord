const puppeteer = require('puppeteer');
const delay = require('delay');

exports.generateBoard = async (pgnString) => {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox'],
    defaultViewport: {
      width: 2560,
      height: 1440,
    },
  });
  const page = await browser.newPage();
  await page.goto('http://localhost:9090/src/page.html', {
    waitUntil: 'load',
    timeout: 0
  });

  page.evaluate(`
    window.chess.import(\`${pgnString}\`);
    window.cr.global.sync(window.chess);
    window.cr.zoom.fullBoard();
  `);

  await delay(500);

  var image = await page.screenshot({
    omitBackground: true
  });

  browser.close();

  return image;
}