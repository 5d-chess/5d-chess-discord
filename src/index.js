const Discord = require('discord.js');
const client = new Discord.Client({ 
  partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
  intents: [ Discord.Intents.FLAGS.GUILD_MESSAGES, Discord.Intents.FLAGS.GUILD_MESSAGE_REACTIONS ],
});
const config = require('../config.json');

const generateBoardFuncs = require('./generateBoard');

client.once('ready', () => {
	console.log('Bot is ready');
});

client.on('message', async (message) => {
  const prefix = config.prefix;
  if(!message.content.startsWith(prefix) || message.author.bot || message.partial) { return null };

  const args = message.content.slice(prefix.length).trim().replace(/\n/g, ' ').split(' ');
  const command = args.shift().toLowerCase();

  var returnMessage = null;
  if(command === 'help') {
    console.log('Showing help message');
    returnMessage = await message.channel.send(`Available commands are:
- \`$help\` - Display this message.
- \`$show <5DPGN string>\` - Generate 5D Chess board based on provided 5DPGN string.`);
  }
  if(command === 'show') {
    var pgnString = message.content.slice(prefix.length + command.length).trim();
    pgnString = pgnString.replace(/\`/g,'');
    pgnString = pgnString.replace(/\n/g,' ');
    pgnString = pgnString.replace(/\]\s/g,']\n').trim();
    console.log(`Generate 5D Chess board: ${pgnString}`);
    try {
      const image = await generateBoardFuncs.generateBoard(pgnString);
      const attachment = new Discord.MessageAttachment(image);
      returnMessage = await message.channel.send(`Generated: \`${pgnString}\``, attachment);
    }
    catch(err) {
      console.error(err);
      returnMessage = await message.channel.send(`Error occurred generating: \`${pgnString}\`
Error message: ${err}`);
    }
  }

  if(returnMessage !== null) { returnMessage.react('❌'); }
  message.delete();
});

client.on('messageReactionAdd', async (reaction, user) => {
  if(reaction.message.partial) { await reaction.message.fetch(); }
  if(reaction.partial) { await reaction.fetch(); }
  if(reaction.message.author.id !== client.user.id) { return null; }
  if(user.bot) { return null; }
  
  const emoji = reaction.emoji.name;
  const message = reaction.message;
  if(emoji === '❌') {
    console.log('Deleting message');
    message.delete();
  }
});

client.login(config.token);
